import Phaser from "phaser";

import imgSky from "./assets/sky.png";
import imgPlatform from "./assets/platform.png";
import imgStar from "./assets/star.png";
import imgBomb from "./assets/bomb.png";
import imgDude from "./assets/dude.png";
import imgFullscreen from "./assets/fullscreen.png";

import imgPlayer from "./assets/player.png";
//import tileMap from "./assets/platformer-simple.json";
//import tileSet from "./assets/tileset.png";

import tileMap from "./assets/tilemap1.json";
import tileSet from "./assets/tileset1.png";

var GameScene = new Phaser.Class({

  Extends: Phaser.Scene,

  initialize:

  function GameScene ()
  {
      Phaser.Scene.call(this, { key: 'gameScene', active: true });

      this.player = null;
      this.cursors = null;
      this.score = 0;
      this.scoreText = null;
      this.map = null;
      this.mapScale = null;

      this.jumpTimer = 0;
      this.isJumping = false;
      this.jumpHeight = 0;
      this.playerPrevPosY = 0;
      this.jumpLandTime = null;

      this.tileMap1 = null;
      this.tileSet1 = null;

      this.timeLastPower;
      this.lastPowerReady = true;
  },

  preload: function ()
  {
      this.load.image('sky', imgSky);
      this.load.image('ground', imgPlatform);
      this.load.image('star', imgStar);
      this.load.image('bomb', imgBomb);
      this.load.spritesheet('dude', imgDude, { frameWidth: 32, frameHeight: 48 });
      this.load.spritesheet('fullscreen', imgFullscreen, { frameWidth: 64, frameHeight: 64 });

      this.load.spritesheet('player', imgDude, { frameWidth: 32, frameHeight: 32, margin: 1, spacing: 2 });
      this.load.tilemapTiledJSON('level1', tileMap);
      this.load.image('gameTiles', tileSet);
  },

  create: function ()
  {
    var map = this.add.tilemap('level1');
    var tiles = map.addTilesetImage("tileset1", "gameTiles");

    //map.createDynamicLayer("Background", tiles);
    this.groundLayer = map.createDynamicLayer("Ground", tiles);
    //map.createDynamicLayer("Foreground", tiles);

    //this.groundLayer.putTileAt(1, 6, 11).properties.collides = true;
    //this.groundLayer.putTileAt(1, 7, 14).properties.collides = true;
    //this.groundLayer.putTileAt(1, 8, 17).properties.collides = true;

    // newTile["collides"] = true;

    /*
    this.map = this.make.tilemap({ key: 'map' });
    var tileset = this.map.addTilesetImage('platformer_tiles');
    var bgLayer = this.map.createDynamicLayer('Background Layer', tileset, 0, 0)
        .setScale(this.mapScale);
    var groundLayer = this.map.createDynamicLayer('Ground Layer', tileset, 0, 0)
        .setScale(this.mapScale);
    var fgLayer = this.map.createDynamicLayer('Foreground Layer', tileset, 0, 0)
        .setScale(this.mapScale)
        .setDepth(1);
    */

    // Set up the layer to have matter bodies. Any colliding tiles will be given a Matter body.
    //groundLayer.setCollisionByProperty({ collides: true });
    //this.matter.world.convertTilemapLayer(groundLayer);

      // this.add.image(400, 300, 'sky');

      /*
      var platforms = this.physics.add.staticGroup();

      platforms.create(400, 568, 'ground').setScale(2).refreshBody();

      platforms.create(600, 400, 'ground');
      platforms.create(50, 250, 'ground');
      platforms.create(750, 220, 'ground');
      */

    this.player = this.physics.add.sprite(100, 450, 'dude');
    this.player.setScale(.7);
    // this.player.setActive(true)

      // Collide the player against the ground layer - here we are grabbing the sprite property from
    // the player (since the Player class is not a Phaser.Sprite).
    
    this.groundLayer.setCollisionByProperty({ collides: true });
    
    this.groundLayer.setCollision([1,2,3]);
    this.physics.world.addCollider(this.player, this.groundLayer);

    this.physics.add.collider(this.player, this.groundLayer);

    //this.cameras.main.startFollow(player.sprite);
    //this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);


    this.physics.world.collide(this.player, tiles, null, this);

      this.player.setBounce(0);
      this.player.setMass(1000);
      this.player.setDragY(50);
      this.player.setCollideWorldBounds(true);

      this.anims.create({
          key: 'left',
          frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
          frameRate: 10,
          repeat: -1
      });

      this.anims.create({
          key: 'turn',
          frames: [ { key: 'dude', frame: 4 } ],
          frameRate: 20
      });

      this.anims.create({
          key: 'right',
          frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
          frameRate: 10,
          repeat: -1
      });

      this.cursors = this.input.keyboard.createCursorKeys();

      var stars = this.physics.add.group({
          key: 'star',
          repeat: 11,
          setXY: { x: 12, y: 0, stepX: 70 }
      });

      stars.children.iterate(function (child) {

          child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));

      });

      this.scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

      //this.physics.add.collider(player, platforms);
      //this.physics.add.collider(stars, platforms);
      this.physics.add.collider(stars, this.groundLayer);

      this.physics.add.overlap(this.player, stars, this.collectStar, null, this);

      //this.player = player;

      var button = this.add.image(800-16, 16, 'fullscreen', 0).setOrigin(1, 0).setInteractive();

      button.on('pointerup', function () {

          if (this.scale.isFullscreen)
          {
              button.setFrame(0);

              this.scale.stopFullscreen();
          }
          else
          {
              button.setFrame(1);

              this.scale.startFullscreen();
          }

      }, this);

      this.scoreText.setText('v15');

      var FKey = this.input.keyboard.addKey('F');

      FKey.on('down', function () {
          console.log('isDown')

          if (this.scale.isFullscreen)
          {
              button.setFrame(0);
              this.scale.stopFullscreen();
          }
          else
          {
              button.setFrame(1);
              this.scale.startFullscreen();
          }

      }, this);
      // this.scale.on('resize', resize, this);

        this.qKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q);
        this.eKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.E);
        
        this.input.keyboard.on('keydown-Q', function (event) {
            //var isDown = this.input.keyboard.checkDown(this.qKey, 500);
            //console.log(isDown)
            if (!this.lastPowerReady) return;
            this.removeTile(-1);
        }, this);

        this.input.keyboard.on('keydown-E', function (event) {
            if (!this.lastPowerReady) return;
            this.removeTile(1);
        }, this);

        this.wKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
        this.aKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.sKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
        this.dKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  },

  restartLastPower: function (tileToReplace) {
    console.log(tileToReplace)
    console.log('READY')
    this.groundLayer.putTileAt(tileToReplace.index, tileToReplace.x, tileToReplace.y);
    
    this.lastPowerReady = true;
  },

  removeTile: function(direction) {
    try {
        var tileUnder = this.groundLayer.getTileAtWorldXY(this.player.x, this.player.y + 20);
        var tileToRemove = this.groundLayer.getTileAt(tileUnder.x + direction, tileUnder.y);
        var tileAbove = this.groundLayer.getTileAt(tileToRemove.x, tileToRemove.y - 1);
        if (tileAbove) return; // we dont physics like that
        
        this.groundLayer.removeTileAt(tileToRemove.x, tileToRemove.y);

        var groundLayer = this.groundLayer;
        setInterval( () => {
            //groundLayer.putTileAt(tileToRemove.index, tileToRemove.x, tileToRemove.y);
        }, 2000);

    } catch(err) {
        return // no tile to remove
    }
    this.lastPowerReady = false;
    // replace block after time
    this.timeLastPower = this.time.delayedCall(2000, this.restartLastPower, [tileToRemove], this);
  },

  update: function ()
  {
    var thisScene = this;
    var cursors = this.cursors;
    var player = this.player;
    // var jumpLandTime = null;
    //var qKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q);
    //var eKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.E);

    // TODO: fix scope?
    //qKey.on('down', (key, event) => {
    // TODO: take out of update function
    
    // TODO: do not change direction in air
      if (cursors.left.isDown || this.aKey.isDown)
      {
          player.setVelocityX(-120);

          player.anims.play('left', true);
      }
      else if (cursors.right.isDown || this.dKey.isDown)
      {
          player.setVelocityX(120);

          player.anims.play('right', true);
      }
      else
      {
          player.setVelocityX(0);

          player.anims.play('turn');
      }

      //console.log('NOW: ' + this.time.now)
      //console.log('LANDTIME: ' + this.jumpLandTime)

      if ((cursors.up.isDown || this.wKey.isDown) && player.body.blocked.down && this.time.now > (this.jumpTimer + 500))
      {
          /* jump key up space */
        this.jumpTimer = this.time.now;
        this.jumpHeight = player.body.y;
        //console.log(this.jumpHeight);
        //this.playerPrevPosY = player.body.y;
        //500
        
        //console.log(this.isJumping);
        //console.log(this.jumpHeight - player.body.y);
        // console.log(this.jumpTimer)
        //console.log(this.isJumping)
        //player.setGravityY(-400);
        player.setVelocityY(-234); // JUMP HEIGHT
        this.isJumping = true;
        player.setGravityY(-100);
      }

      // reset isJumping after player lands
      if(player.body.blocked.down && this.isJumping && this.time.now > (this.jumpTimer + 100)){
          // debugger
          console.log('LAND')
          player.setGravityY(0);
          this.isJumping = false;
      }
      

    //console.log(player.body.gravity.y);

      if (player.body.gravity != 0 && player.body.y > this.playerPrevPosY) {
        //console.log('DOWN')
        //player.setGravityY(1000);
      }
      this.playerPrevPosY = player.body.y;
      //console.log(this.jumpHeight - player.body.y);
      //console.log(this.isJumping);


    if (player.body.blocked.down && this.time.now > (this.jumpTimer + 100)) {
        //console.log('LAND')
        /*
        if(this.isJumping == true) {
            this.jumpLandTime = this.time.now;
            console.log('LAND')
            console.log(this.jumpLandTime)
        }
        if (this.time.now > (this.jumpTimer + 100)) {
            this.isJumping = false;
        }
        */
        //console.log(this.jumpLandTime);
        //console.log(this.jumpLandTime);
    }
    
      /*
      if(!player.body.blocked.down) {
          console.log('JUMPING')
        if (this.time.now < (this.jumpTimer + 900)) {
            console.log('jump')
        } else if (this.time.now > (this.jumpTimer + 900)) {
            player.setVelocityY(200);
            console.log('hangtime')
        } else {
            console.log('done')
        }
      }
      */
        
  },

  collectStar: function (player, star)
  {
      star.disableBody(true, true);

      this.score += 10;
      this.scoreText.setText('Score: ' + this.score);
  }

});

var config = {
  type: Phaser.AUTO,
  scale: {
      mode: Phaser.Scale.FIT,
      parent: 'phaser-example',
      autoCenter: Phaser.Scale.CENTER_BOTH,
      width: 800,
      height: 600
  },
    physics: {
        default: 'arcade',
        arcade: {
        gravity: { y: 800 },
        debug: false
        }
    },
  /*
  physics: {
    default: 'matter',
    matter: {
        gravity: { y: 1 },
        enableSleep: false
    }
  },
  */
  scene: GameScene
};

var game = new Phaser.Game(config);

/*
function resize (gameSize, baseSize, displaySize, resolution)
{
    var width = gameSize.width;
    var height = gameSize.height;

    this.cameras.resize(width, height);

    this.bg.setSize(width, height);
    this.logo.setPosition(width / 2, height / 2);
}
*/